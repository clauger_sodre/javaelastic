package com.springelastic.springelastic.repository;

import com.springelastic.springelastic.model.Product;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Qualifier("Product")
@Repository
public interface ProductRepository extends CrudRepository<Product,Integer>{


}

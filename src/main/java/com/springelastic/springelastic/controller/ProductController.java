package com.springelastic.springelastic.controller;

import com.springelastic.springelastic.model.Product;
import com.springelastic.springelastic.repository.ProductRepository;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value="/product")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping()
    Product create(@RequestBody Product product){
        return productRepository.save(product);
    }


    @GetMapping("/{id}")
    public Optional<Product> findById(@PathVariable Integer id) {
        return productRepository.findById(id);
    }

    @GetMapping
    public List<Product> Product() {
        return (List<Product>) productRepository.findAll();
    }


}
